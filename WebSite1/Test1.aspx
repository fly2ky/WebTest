﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Flat UI Free</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="flat-ui/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI -->
    <link href="flat-ui/css/flat-ui.css" rel="stylesheet">

    <link rel="shortcut icon" href="flat-ui/img/favicon.ico">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="flat-ui/js/vendor/html5shiv.js"></script>
      <script src="flat-ui/js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
    <div class="container">  
            <h4>Form</h4>
            <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputPassword2">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
            </div>
            <div class="form-group">
                <label class="checkbox" for="checkbox2">
                <input type="checkbox" data-toggle="checkbox" value="" id="checkbox2">
                Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-default">Sign in</button>
            </form>
    </div>
    <!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="flat-ui/js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="flat-ui/js/flat-ui.min.js"></script>

    <script src="js/application.js"></script>
</body>
</html>

