﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Flat UI Free</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="flat-ui/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI -->
    <link href="flat-ui/css/flat-ui.css" rel="stylesheet">

    <link rel="shortcut icon" href="flat-ui/img/favicon.ico">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="flat-ui/js/vendor/html5shiv.js"></script>
      <script src="flat-ui/js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
    <div class="container">  
      <div class="row demo-row">
        <div class="col-xs-3">
          <a href="Test1.aspx" class="btn btn-block btn-lg btn-primary">Test1</a>
        </div>
        <div class="col-xs-3">
          <a href="Test2.aspx" class="btn btn-block btn-lg btn-warning">Test2</a>
        </div>
        <div class="col-xs-3">
          <a href="#fakelink" class="btn btn-block btn-lg btn-default">Default Button</a>
        </div>
        <div class="col-xs-3">
          <a href="#fakelink" class="btn btn-block btn-lg btn-danger">Danger Button</a>
        </div>
      </div> <!-- /row -->

      <div class="row demo-row">
        <div class="col-xs-3">
          <a href="#fakelink" class="btn btn-block btn-lg btn-success">Success Button</a>
        </div>
        <div class="col-xs-3">
          <a href="#fakelink" class="btn btn-block btn-lg btn-inverse">Inverse Button</a>
        </div>
        <div class="col-xs-3">
          <a href="#fakelink" class="btn btn-block btn-lg btn-info">Info Button</a>
        </div>
        <div class="col-xs-3">
          <a href="#fakelink" class="btn btn-block btn-lg btn-default disabled">Disabled Button</a>
        </div>
      </div> <!-- /row -->
    </div>
    <!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="flat-ui/js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="flat-ui/js/flat-ui.min.js"></script>

    <script src="js/application.js"></script>
</body>
</html>
