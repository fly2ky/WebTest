﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Flat UI Free</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Loading Bootstrap -->
    <link href="flat-ui/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!-- Loading Flat UI -->
    <link href="flat-ui/css/flat-ui.css" rel="stylesheet">
    <link rel="shortcut icon" href="flat-ui/img/favicon.ico">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="flat-ui/js/vendor/html5shiv.js"></script>
      <script src="flat-ui/js/vendor/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>Flat UI Free</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <!-- Loading Bootstrap -->
            <link href="flat-ui/css/vendor/bootstrap.min.css" rel="stylesheet">
            <!-- Loading Flat UI -->
            <link href="flat-ui/css/flat-ui.css" rel="stylesheet">
            <link rel="shortcut icon" href="flat-ui/img/favicon.ico">
            <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
            <!--[if lt IE 9]>
      <script src="flat-ui/js/vendor/html5shiv.js"></script>
      <script src="flat-ui/js/vendor/respond.min.js"></script>
    <![endif]-->
        </head>
        <body>
            <div class="container">
                <form action="#">
                <div class="login-form">
                    <div class="form-group">
                        <input type="text" class="form-control login-field" value="" placeholder="Enter your name"
                            id="login-name" />
                        <label class="login-field-icon fui-user" for="login-name">
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control login-field" value="" placeholder="Password"
                            id="login-pass" />
                        <label class="login-field-icon fui-lock" for="login-pass">
                        </label>
                    </div>
                    <a class="btn btn-primary btn-lg btn-block" href="#">Log in</a> <a class="login-link"
                        href="#">Lost your password?</a>
                </div>
                </form>
            </div>
            <!-- /.container -->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="flat-ui/js/vendor/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="flat-ui/js/flat-ui.min.js"></script>
            <script src="js/application.js"></script>
        </body>
        </html>
    </div>
    <!-- /.container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="flat-ui/js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="flat-ui/js/flat-ui.min.js"></script>
    <script src="js/application.js"></script>
</body>
</html>
