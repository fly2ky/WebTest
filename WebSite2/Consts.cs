﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTest.Admin
{
    public static class Consts
    {
        public const string OAConnectionName = "HisOA";
        public const string SessionUserId = "Session_UserId";
        
        public const string CacheDict = "CacheDict";
        public const string CacheSqlDict = "CacheSqlDict";
        public const int CacheMinute = 30;
        
    }
}