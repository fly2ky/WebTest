﻿<%@ Page Language="C#"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="/js/jquery-1.4.1.min.js"></script>

</head>
<body>
<div>

    <a href="/Page1.do" >PageResult</a> <br/>
    <a href="/Page3.do" >RedirectResult</a> <br/>
    <a href="/Page4.do" >XmlResult</a> <br/>
    <a href="/Page5.do" >JsonResult</a> <br/>
    <img src="/Page6.do" ><br/>

    <input type="button" id="btnAdd1" value="Ajax 计算 1 + 2" /> <br/>
    <input type="button" id="btnXml" value="Ajax XmlResult" /> <br/>
    <input type="button" id="btnJson" value="Ajax JsonResult" /> <br/>


    <a href="/Login1.do" >Login-flatUI</a> <br/>
    <a href="/Login2.do" >Login-BootStrap</a> <br/>

    <a href="dwz_jui/index.html">DWZ-UI</a><br/>
    <a href="OA/login.aspx">OADemo</a>
</div>

<p><b>服务器返回的结果：</b></p>
<textarea id="output" cols="20" rows="50" style="width: 90%; height: 200px"></textarea>

<script type="text/javascript">
    $(function () {
        $("#btnAdd1").click(function () {
            $.ajax({
                type: "POST",
                url: "/Ajax.do",
                data: { b: 2, a: 1 },
                success: function (result) {
                    $("#output").val(result);
                }
            });
        });


        $("#btnXml").click(function () {
            $.ajax({
                type: "POST",
                url: "Page4.do",
                data: {},
                dataType: "text",  //xml,html,text,json
                success: function (result) {
                    $("#output").val(result);
                }
            });
        });

        $("#btnJson").click(function () {
            $.ajax({
                type: "POST",
                url: "Page5.do",
                data: {},
                dataType: "text",  //xml,html,text,json
                success: function (result) {
                    $("#output").val(result);
                }
            });
        });


    });

</script>
</body>
</html>
