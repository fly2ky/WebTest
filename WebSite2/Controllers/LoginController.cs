﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using HisNet.Web;
using HisNet.Web.Context;
using HisNet.Web.Action;
using HisNet.Web.View;

using WebTest.Admin;


namespace WebTest
{
    public class JsonMessage
    {
        public int code;
        public string message;
        public string result;
    }

    
    public class LoginController
    {
        [Action]
        [PageUrl(Url = "/Login1.do")]
        public IActionResult Index1()
        {
            return new RedirectResult("/Test1/Login1.aspx");
        }

        [Action]
        [PageUrl(Url = "/Login2.do")]
        public IActionResult Index2()
        {
            return new RedirectResult("/Test1/Login2.aspx");
        }


        [Action]
        [PageUrl(Url = "/Login.do")]
        public IActionResult Login1(string user,string password)
        {
            JsonMessage jm = new JsonMessage();
            if (user != "admin" || password != "1")
            {              
                jm.code = -1;
                jm.message = "登陆失败！";
                jm.result = "";
                
            }
            else
            {
                jm.code = 1;
                jm.message = "登陆成功！";
                jm.result = "/Test1/AdminMain.aspx";
            }

            return new JsonResult(jm);           
        }

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/Login.do")]
        public IActionResult Login(string user, string password)
        {
            //验证用户有没有登陆，
                        
            JsonMessage jm = new JsonMessage();
            try
            {
                if (QXManager.Instance.Login(user, password))
                {
                    jm.code = 1;
                    jm.message = "登陆成功！";
                    jm.result = "/OA/Main.do";

                }
                else
                {
                    jm.code = -1;
                    jm.message = "登陆失败！";
                    jm.result = "";
                }
            }
            catch(Exception e){
                jm.code = -99;
                jm.message = e.Message;
                jm.result = "";
            }
            return new JsonResult(jm);     

        }

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/ReLogin.do")]
        public IActionResult ReLogin(string user, string password)
        {
            return new PageResult("/OA/relogin.aspx", null);
        }

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/Main.do")]
        public IActionResult MainIndex()
        {
            //验证有无权限
            if (!QXManager.Instance.LoginFlag)
            {
                return new RedirectResult("/OA/login.aspx");
            }
            else
            {
                ZDRY zdry = QXManager.Instance.CurrentRY;

                return ChangeJS(zdry.jsbm);
                
            }
        }

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/logout.do")]
        public IActionResult Logout()
        {
            if (QXManager.Instance.LoginFlag)
            {
                QXManager.Instance.Logout();
            }
            
            return new RedirectResult("/OA/login.aspx");
        }

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/changeJS.do")]
        public IActionResult ChangeJS(string jsbm)
        {
            //验证有无权限
            if (!QXManager.Instance.LoginFlag)
            {
                return new RedirectResult("/OA/login.aspx");
            }
            else
            {
                DataAdapter da = new DataAdapter();
                
                //人员信息
                ZDRY zdry = QXManager.Instance.CurrentRY;
                zdry.jsbm = jsbm;
                zdry.jsmc = ZDManager.Instance.GetDicMC("ZD_JS",jsbm);
                da.Add("loginRY", zdry);

                //获取角色列表
                DataTable dtJS = QXManager.Instance.GetJSList(zdry.bm);
                da.Add("dtJS", dtJS);


                //获取横向导航条
                DataTable dtJSXT = QXManager.Instance.GetJSXTList(zdry.jsbm, zdry.bm);
                da.Add("dtJSXT", dtJSXT);

                //功能菜单权限
                DataTable dtJSGN = QXManager.Instance.GetJSGNList(zdry.jsbm, zdry.bm);
                da.Add("dtJSGN", dtJSGN);
                


                return new PageResult("/OA/main.aspx", da);
            }
        }

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/changeJSXT.do")]
        public IActionResult changeJSXT(string jsbm)
        {
            //验证有无权限
            if (!QXManager.Instance.LoginFlag)
            {
                return new RedirectResult("/OA/login.aspx");
            }
            else
            {
                DataAdapter da = new DataAdapter();
                //人员信息
                ZDRY zdry = QXManager.Instance.CurrentRY;



                return new PageResult("/OA/menu.aspx", da);
            }

        }

    }
}