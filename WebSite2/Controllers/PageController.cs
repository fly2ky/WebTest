﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;

using HisNet.Web;
using HisNet.Web.Context;
using HisNet.Web.Action;

namespace WebTest
{
    public class Page1Model
    {
        public string bm;
        public string mc;
    }   
    
    public class PageController
    {
        [Action]
        [PageUrl(Url = "/Page1.do")]
        public object Page1()
        {
            //string filePath = HttpContextHelper.RequestFilePath;
            //int p = filePath.LastIndexOf('/');
            //string pageName = filePath.Substring(p + 1);
            string pageName = "/Pages/Page1.aspx";

            Page1Model pm = new Page1Model();
            pm.bm = "BM";
            pm.mc = "sadsdsds";

            return new PageResult(pageName, pm);
        }

        [Action]
        [PageUrl(Url = "/Ajax.do")]
        public int Add(int a, int b)
        {
            return a + b;
        }


        [Action]
        [PageUrl(Url = "/Page3.do")]
        public object Page3()
        {
            return new RedirectResult("/Pages/Page3.aspx");
        }

        [Action]
        [PageUrl(Url = "/Page4.do")]
        public object Page4()
        {
            Page1Model pm = new Page1Model();
            pm.bm = "BM";
            pm.mc = "MC";

            return new XmlResult(pm);
        }


        [Action]
        [PageUrl(Url = "/Page5.do")]
        public object Page5()
        {
            Page1Model pm = new Page1Model();
            pm.bm = "BM";
            pm.mc = "MC";

            return new JsonResult(pm);
        }

        [Action]
        [PageUrl(Url = "/Page6.do")]
        public object Page6()
        {
            Bitmap image = new Bitmap(150,30);
            using (Graphics graphics = Graphics.FromImage(image))
            {
                graphics.Clear(Color.White);
                Font font = new Font("宋体", 14f, FontStyle.Bold);
                Brush brush = new SolidBrush(Color.Blue);
                graphics.DrawString("StreamResult", font, brush, 0, 0);

            }

            MemoryStream stream = new MemoryStream();
            image.Save(stream, System.Drawing.Imaging.ImageFormat.Gif);
            stream.Position = 0;

            return new StreamResult("gif", stream);
        }

    }
}