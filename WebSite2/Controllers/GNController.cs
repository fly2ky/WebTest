﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using HisNet.Web;
using HisNet.Web.Context;
using HisNet.Web.Action;
using HisNet.Web.View;

using WebTest.Admin;

namespace WebTest.Controllers
{
    public class GNController
    {

        [SessionMode(SessionMode.Support)]
        [Action]
        [PageUrl(Url = "/OA/openGN.do")]
        public IActionResult openGN(string gnbm,string xtbm)
        {
            //验证有无权限
            if (!QXManager.Instance.LoginFlag)
            {
                return new RedirectResult("/OA/login.aspx");
            }
            else
            {
                //获取URL
                DataTable dtGN = ZDManager.Instance.GetDictData("ZD_GN");
                if (dtGN != null && dtGN.Rows.Count > 0)
                {
                    DataRow[] dr = dtGN.Select(string.Format("XTBM='{0}' AND BM='{1}'", xtbm, gnbm));
                    if (dr.Count() > 0)
                    {
                        string sURL = dr[0]["URL"].ToString();
                        if (sURL.Length > 0)
                        {
                            return new RedirectResult(sURL);

                        }
                    }                    
                }

                return new RedirectResult("/OA/404.aspx");
              
            }
        }
    }
}