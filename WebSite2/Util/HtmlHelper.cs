﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebTest
{
    public static class HtmlHelper
    {

        public static string InnerForGnMenu(DataTable dataTable, string gnbm, string xtbm)
        {
            string result = "";
            string bm = "";
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                System.Data.DataRow[] rows;

                rows = dataTable.Select(string.Format("SJBM='{0}' AND XTBM='{1}' ", gnbm, xtbm), "BM asc");
                foreach (System.Data.DataRow row in rows)
                {

                    result += "<li><a";
                    if (string.IsNullOrEmpty(row["SFMX"].ToString()))
                    {
                        result += string.Format(">{0}</a>",row["MC"].ToString());
                    }
                    else
                    {
                        string target = row["LX"].ToString();
                        if (string.IsNullOrEmpty(row["LX"].ToString()))
                        {
                            target = "navTab";
                        }
                        result += string.Format(" href=\"/OA/openGN.do?gnbm={1}&xtbm={2}\" target=\"{3}\" rel=\"{4}\">{0}</a>",
                            row["MC"].ToString(), row["BM"].ToString(), xtbm,target,row["BM"].ToString());
                    }

                    bm = row["BM"].ToString();
                    System.Data.DataRow[] rows1 = dataTable.Select(string.Format("SJBM='{0}' AND XTBM='{1}' ", bm, xtbm), "BM asc");
                    if (rows1.Length > 0)
                    {
                        string mx = InnerForGnMenu(dataTable, bm, xtbm);
                        result += string.Format("<ul>{0}</ul>",mx);
                    }


                    result += "</li>";
                }
            }

            return result;

        }
    }
}