﻿<%@ Page Language="C#" AutoEventWireup="false" Inherits="HisNet.Web.View.PageView" %>

<%
DataAdapter da = Model as DataAdapter;
%>
<div class="pageHeader">
    <div class="panelBar">
        <ul class="toolBar">
            <li><a class="icon" href="javascript:" id="btnRefresh"><span>刷新</span></a></li>
            <li><a class="icon" href="javascript:" id="btnAdd"><span>新增</span></a></li>
            <li><a class="icon" href="javascript:" id="btnSave"><span>保存</span></a></li>
            <li><a class="icon" href="javascript:" id="btnDelete"><span>删除</span></a></li>
            <li class="line">line</li>
            <li><a class="icon" href="javascript:" id="btnExport"><span>导出</span></a></li>
        </ul>
    </div>
</div>
<%=da["gnbm"] %>
