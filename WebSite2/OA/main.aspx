﻿<%@ Page Language="C#" AutoEventWireup="false" Inherits="HisNet.Web.View.PageView" %>


<%
DataAdapter da = Model as DataAdapter;
ZDRY zdry = da["loginRY"] as ZDRY;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

<link href="/dwz_jui/themes/default/style.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/dwz_jui/themes/css/core.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/dwz_jui/themes/css/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="/dwz_jui/uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen"/>
<!--[if IE]>
<link href="themes/css/ieHack.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->
<style type="text/css">
	#header{height:85px}
	#leftside, #container, #splitBar, #splitBarProxy{top:90px}
</style>

<!--[if lt IE 9]><script src="/dwz_jui/js/speedup.js" type="text/javascript"></script><script src="/dwz_jui/js/jquery-1.11.3.min.js" type="text/javascript"></script><![endif]-->
<!--[if gte IE 9]><!--><script src="/dwz_jui/js/jquery-2.1.4.min.js" type="text/javascript"></script><!--<![endif]-->

<script src="/dwz_jui/js/jquery.cookie.js" type="text/javascript"></script>
<script src="/dwz_jui/js/jquery.validate.js" type="text/javascript"></script>
<script src="/dwz_jui/js/jquery.bgiframe.js" type="text/javascript"></script>
<script src="/dwz_jui/xheditor/xheditor-1.2.2.min.js" type="text/javascript"></script>
<script src="/dwz_jui/xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
<script src="/dwz_jui/uploadify/scripts/jquery.uploadify.min.js" type="text/javascript"></script>

<script src="/dwz_jui/js/dwz.core.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.util.date.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.validate.method.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.barDrag.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.drag.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.tree.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.accordion.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.ui.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.theme.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.switchEnv.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.alertMsg.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.contextmenu.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.navTab.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.tab.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.resize.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.dialog.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.dialogDrag.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.sortDrag.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.cssTable.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.stable.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.taskBar.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.ajax.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.pagination.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.database.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.datepicker.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.effects.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.panel.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.checkbox.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.history.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.combox.js" type="text/javascript"></script>
<script src="/dwz_jui/js/dwz.print.js" type="text/javascript"></script>

<!-- 可以用dwz.min.js替换前面全部dwz.*.js (注意：替换时下面dwz.regional.zh.js还需要引入)
<script src="/dwz_jui/js/dwz.min.js" type="text/javascript"></script>
-->
<script src="/dwz_jui/js/dwz.regional.zh.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        DWZ.init("/dwz_jui/dwz.frag.xml", {
            loginUrl: "/OA/ReLogin.do", loginTitle: "登录", // 弹出登录对话框
            //		loginUrl:"login.html",	// 跳到登录页面
            statusCode: { ok: 200, error: 300, timeout: 301 }, //【可选】
            keys: { statusCode: "statusCode", message: "message" }, //【可选】
            pageInfo: { pageNum: "pageNum", numPerPage: "numPerPage", orderField: "orderField", orderDirection: "orderDirection" }, //【可选】
            debug: false, // 调试模式 【true|false】
            callback: function () {
                initEnv();
                $("#themeList").theme({ themeBase: "/dwz_jui/themes" });
                setTimeout(function () { $("#sidebar .toggleCollapse div").trigger("click"); }, 10);
            }
        });

    });
</script>
</head>
<body scroll="no">
	<div id="layout">

        <!-- 头部  -->
		<div id="header">
			<div class="headerNav">
				
				<ul class="nav">
                    <li><a href="#"><%=zdry.mc %>，你好！</a></li>
					<li id="switchEnvBox">
                        <a href="javascript:">（<span><%=zdry.jsmc%></span>）切换角色</a>
						<ul>
						<%
                            System.Data.DataTable dtJS = da["dtJS"] as System.Data.DataTable;
                            if (dtJS != null && dtJS.Rows.Count > 0)
                       {
                           foreach (System.Data.DataRow row in dtJS.Rows)
                            {
                        %>
                            <li><a href="changeJS.do?jsbm=<%=row["BM"] %>"><%=row["MC"] %></a></li>

                        <%
                            }       
                        }
                         %>
					    </ul>
					</li>
					<li><a href="changepwd.html" target="dialog" width="600">设置</a></li>
                    <li><a href="Logout.do">退出</a></li>
				</ul>
				<ul class="themeList" id="themeList">
					<li theme="default"><div class="selected">蓝色</div></li>
					<li theme="green"><div>绿色</div></li>
					<!--<li theme="red"><div>红色</div></li>-->
					<li theme="purple"><div>紫色</div></li>
					<li theme="silver"><div>银色</div></li>
					<li theme="azure"><div>天蓝</div></li>
				</ul>
			</div>
		
			<div id="navMenu">
				<ul>
					<%
                        System.Data.DataTable dtJSXT = da["dtJSXT"] as System.Data.DataTable;
                        if (dtJSXT != null && dtJSXT.Rows.Count > 0)			
                    {
                        int iRow = 0;
                        foreach (System.Data.DataRow row in dtJSXT.Rows)
                        {
                            if (iRow == 0)
                            {                              		
					%>                       
                        <li class="selected"><a href="changeJSXT.do?bm=<%=row["BM"] %>"><span><%=row["MC"]%></span></a></li>

                    <%
                            }
                            else
                            {
                    %>
                       <li><a href="changeJSXT.do?bm=<%=row["BM"] %>"><span><%=row["MC"]%></span></a></li>
                    <%
                            }
                        }       
                    }
                    %>                    
				</ul>
			</div>
		</div>

        <!-- 左边  -->
		<div id="leftside">
			<div id="sidebar_s">
				<div class="collapse">
					<div class="toggleCollapse"><div></div></div>
				</div>
			</div>
			<div id="sidebar">
				<div class="toggleCollapse"><h2>主菜单</h2><div>收缩</div></div>

				<div class="accordion" fillSpace="sidebar">
                    <% 
                       System.Data.DataTable dtJSGN = da["dtJSGN"] as System.Data.DataTable;
                       if (dtJSGN != null && dtJSGN.Rows.Count > 0)
                       {
                           System.Data.DataRow[] rows = dtJSGN.Select("SJBM=''", "XTBM ASC,BM ASC");
                           foreach (System.Data.DataRow row in rows)
                           {
                               string sXTBM = row["XTBM"].ToString();
                               string sGNBM = row["BM"].ToString();
                    %>
					<div class="accordionHeader">
						<h2><span>Folder</span><%=row["MC"].ToString() %></h2>
					</div>
                    <div class="accordionContent">
                        <ul class="tree treeFolder expand">
                            <%=HtmlHelper.InnerForGnMenu(dtJSGN, sGNBM, sXTBM)%>
                        </ul>
                    </div>

                    <%
                           }
                       }
                    %>

				</div>

			</div>
		</div>

        <!-- 中间区域  -->
		<div id="container">
			<div id="navTab" class="tabsPage">
				<div class="tabsPageHeader">
					<div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
						<ul class="navTab-tab">
							<li tabid="main" class="main"><a href="javascript:;"><span><span class="home_icon">我的主页</span></span></a></li>
						</ul>
					</div>
					<div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
					<div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
					<div class="tabsMore">more</div>
				</div>
				<ul class="tabsMoreList">
					<li><a href="javascript:;">我的主页</a></li>
				</ul>
				<div class="navTab-panel tabsPageContent layoutBox">
					<div class="page unitBox">
						<div class="accountInfo">
							<div class="alertInfo">

							</div>
							<div class="right">

							</div>
						</div>
						<div class="pageFormContent" layoutH="80">
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</body>
</html>
