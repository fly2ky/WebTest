﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using HisNet.Data;
using HisNet.Web.Cache;

namespace WebTest.Admin
{
    public class QXManager
    {
        private static QXManager instance = new QXManager();

        private QXManager()
        {
        }

        public static QXManager Instance
        {
            get
            {
                return instance;
            }
        }

        public bool LoginFlag
        {
            get
            {
                ZDRY zdry = null;
                if (HttpContext.Current.Session[Consts.SessionUserId] != null)
                {
                    zdry = HttpContext.Current.Session[Consts.SessionUserId] as ZDRY;
                }

                return zdry != null;
            }
        }

        public bool Login(string user, string password)
        {
            bool re = false;
            using (DbSession dbSession = new DbSession(Consts.OAConnectionName))
            {                
                string sql = string.Format(@"SELECT RY.BM AS RYBM,RY.MC AS RYMC,RY.JSBM,RY.PASS,JS.MC AS JSMC 
                                            FROM ZD_RY RY
                                            LEFT JOIN ZD_JS JS ON RY.JSBM=JS.BM
                                            WHERE RY.BM='{0}' ", user);
                DataTable dt = dbSession.Query(sql);
                if (dt.Rows.Count > 0 && dt.Rows[0]["PASS"].ToString() == password)
                {
                    ZDRY zdry = new ZDRY();
                    zdry.bm = user;
                    zdry.mc = dt.Rows[0]["RYMC"].ToString();
                    zdry.jsbm = dt.Rows[0]["JSBM"].ToString();
                    zdry.jsmc = dt.Rows[0]["JSMC"].ToString();
                    HttpContext.Current.Session[Consts.SessionUserId] = zdry;
                    re = true;                   
                }

            }
            return re;
        }

        public ZDRY CurrentRY
        {
            get
            {
                ZDRY zdry = null;
                if (HttpContext.Current.Session[Consts.SessionUserId] != null)
                {
                    zdry = HttpContext.Current.Session[Consts.SessionUserId] as ZDRY;
                    if (zdry == null)
                    {
                        throw new Exception("Session过期或丢失， 请重新登入");
                    }
                }

                return zdry;
            }
        }


        public void Logout()
        {
            if (HttpContext.Current.Session[Consts.SessionUserId] != null)
            {
                HttpContext.Current.Session.Clear();
            }

        }

        /// <summary>
        /// 获取角色列表，并缓存
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public DataTable GetJSList(string user)
        {
            string sql = @"SELECT RY.RYBM,JS.BM,JS.MC FROM ZD_RYJS RY,ZD_JS JS 
                                    WHERE RY.JSBM=JS.BM ";
            
            DataTable dt = ZDManager.Instance.GetSQLDictData("ZD_RYJS",sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow[] dr = dt.Select(string.Format("RYBM='{0}'", user));
                if (dr.Count() > 0)
                {
                    return dr.CopyToDataTable();
                }
                else
                {
                    return null;
                }               
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// 获取角色所拥有系统权限
        /// </summary>
        /// <param name="jsbm"></param>
        /// <param name="rybm"></param>
        /// <returns></returns>
        public DataTable GetJSXTList(string jsbm, string rybm)
        {

            string sql = @"SELECT DISTINCT RY.RYBM,JS.JSBM,XT.*
                            FROM ZD_RYJS RY,ZD_JSGN JS,ZD_XT XT
                            WHERE RY.JSBM = JS.JSBM AND JS.XTBM = XT.BM ";

            DataTable dt = ZDManager.Instance.GetSQLDictData("ZD_JSXT", sql);

            if (dt != null && dt.Rows.Count>0)
            {
                DataRow[] dr = dt.Select(string.Format("RYBM='{0}' AND JSBM='{1}'", rybm, jsbm));
                if (dr.Count() > 0)
                {
                    return dr.CopyToDataTable();
                }
                else
                {
                    return null;
                }                 
                
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// 获取角色功能列表
        /// </summary>
        /// <param name="jsbm"></param>
        /// <param name="rybm"></param>
        /// <returns></returns>
        public DataTable GetJSGNList(string jsbm,string rybm)
        {

            string sql = @"SELECT RY.RYBM,JS.JSBM,GN.*
                            FROM ZD_RYJS RY,ZD_JSGN JS,ZD_GN GN
                            WHERE RY.JSBM = JS.JSBM AND JS.XTBM = GN.XTBM AND JS.GNBM = GN.BM";

            DataTable dt = ZDManager.Instance.GetSQLDictData("ZD_JSGN", sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow[] dr = dt.Select(string.Format("RYBM='{0}' AND JSBM='{1}'", rybm, jsbm));
                if (dr.Count() > 0)
                {
                    return dr.CopyToDataTable();
                }
                else
                {
                    return null;
                }                 
                
            }
            else
            {
                return null;
            }


        }

    }
}