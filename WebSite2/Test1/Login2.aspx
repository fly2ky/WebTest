﻿<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8" />
    <title>BootStrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Loading Bootstrap -->
    <link href="/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container">
        <div class="form-login">
        <h2 class="form-login-heading">请登陆</h2>
            <div class="form-group">
                <input type="text" class="form-control" value="" placeholder="用户名"
                    id="login-name" name="user" autofocus />
                <label class="sr-only" for="login-name"/>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" value="" placeholder="密码"
                    id="login-pass" name="password" />
                <label class="sr-only" for="login-pass" />
            </div>
            <a class="btn btn-primary btn-lg btn-block" href="#" id="btnCommit">登陆</a> 
           
            <a class="btn btn-link" href="#">忘记密码?</a>
            <div id="message" class="alert alert-warning">
            </div>
        </div>
    </div>
    <!-- /.container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#btnCommit").click(function () {
                $("#message").html("");
                var usr = $("#login-name").val();
                var pas = $("#login-pass").val();

                $.ajax({
                    type: "POST",
                    url: "/Login.do",
                    data: { user: usr, password: pas },
                    dataType: "json",  //xml,html,text,json
                    success: function (result) {
                        //alert(result.message);
                        if (result.code == "1") {
                            window.location = result.result;
                        } else {
                            $("#message").html(result.message);
                        }

                    }
                });


            });

        });

    </script>
</body>
</html>
